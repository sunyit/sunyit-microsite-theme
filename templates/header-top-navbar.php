<header class="banner" role="banner">

  <div class="container">

    <nav id="primary-navigation" class="navbar navbar-sunyit navbar-static-top" role="navigation">
			
			<div class="navbar-header">
				<button class="navbar-btn btn btn-primary btn-outline btn-lg" data-toggle="collapse" data-target=".navbar-collapse">
					Menu
				</button>
				<a class="navbar-brand" href="/"><?php bloginfo('name'); ?></a>		
			</div>
    
      <div class="collapse navbar-collapse">

				<div class="navbar-sidebar">
					<h1 class="section-title">
					  <?php if ($title = get_section_title( get_queried_object_id() )): ?>
					    <?php echo $title; ?>
					  <?php else: ?>
					    <?php echo bloginfo('name'); ?>
					  <?php endif; ?>
					</h1>
				</div>

        <?php
          $nav_options = array(
            	'menu_class'				 => 'nav navbar-nav',
							'container'         => 'div',
							'container_class'   => 'collapse navbar-collapse',
							'container_id'      => 'bs-example-navbar-collapse-1',
							'menu_class'        => 'nav navbar-nav',
							'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
							'walker'            => new wp_bootstrap_navwalker()
          );
        	if ($menu_id = get_section_menu( get_queried_object_id() ) ) :
        	  $nav_options['menu'] = $menu_id;
          else:
						if (has_nav_menu('primary_navigation')) :
              $nav_options['theme_location'] = 'primary_navigation';
							//wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
						endif;
          endif;        	
          wp_nav_menu($nav_options);
        ?>
        
      </div>

    </nav>

  </div>

</header>
