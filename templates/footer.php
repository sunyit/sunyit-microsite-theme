<footer class="content-info" role="contentinfo">
  <div class="container">
    <div class="row">
      <div class="col-lg-9 col-lg-push-3">
        <!--<div class="<?php echo roots_main_class(); ?>">-->
        <?php dynamic_sidebar('sidebar-footer'); ?>
      </div>
      <div class="col-lg-3 col-lg-pull-9">
        <!--<div class="<?php echo roots_sidebar_class(); ?>">-->
        <?php dynamic_sidebar('sidebar-footer-secondary'); ?>
      </div>
    </div>
  </div>
  <p class="copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('description'); ?>. All rights reserved.</p>
</footer>

<?php wp_footer(); ?>

