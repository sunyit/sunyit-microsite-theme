<?php
/**
 * Custom functions
 */

require_once('wp_bootstrap_navwalker.php');

/**
 * Check if a post is a custom post type.
 * http://wordpress.stackexchange.com/questions/6731/if-is-custom-post-type
 * @param  mixed $post Post object or ID
 * @return boolean
 */
function is_custom_post_type( $post = NULL )
{
    $all_custom_post_types = get_post_types( array ( '_builtin' => FALSE ) );

    // there are no custom post types
    if ( empty ( $all_custom_post_types ) )
        return FALSE;

    $custom_types      = array_keys( $all_custom_post_types );
    $current_post_type = get_post_type( $post );

    // could not detect current type
    if ( ! $current_post_type )
        return FALSE;

    return in_array( $current_post_type, $custom_types );
}

function sunyit_widgets_init() {

  register_sidebar(array(
    'name'          => __('Header', 'roots'),
    'id'            => 'sidebar-header',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ));

  register_sidebar(array(
    'name'          => __('Footer Secondary', 'roots'),
    'id'            => 'sidebar-footer-secondary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ));
}

add_action('widgets_init', 'sunyit_widgets_init');


// disable inline CSS styles for image galleries
add_filter( 'use_default_gallery_style', '__return_false' );


/**
 * http://premium.wpmudev.org/blog/daily-tip-how-to-embed-rss-feed-entries-in-wordpress-posts/
 */

include_once(ABSPATH.WPINC.'/class-feed.php');

function fetch_feed_with_options($options=array()) {

	extract(shortcode_atts(array(
		"url" => 'http://',
		"num"  => '10',
	), $options));

	add_filter( 'wp_feed_cache_transient_lifetime', 'get_homepage_news_feed_lifetime' );
	$feed = fetch_feed($url);
	remove_filter( 'wp_feed_cache_transient_lifetime', 'get_homepage_news_feed_lifetime' );

	foreach ($options as $key => $val) {
		if (strpos($key,'simplepie_') === 0) {
			$func = substr($key,10);
			$feed->$func($val);
		}
	}
	
	return $feed->get_items(0,$num);
	
}
add_shortcode('feed', 'fetch_feed_with_options');

function get_homepage_news_feed_lifetime( $seconds ) {
	return 900; // 15 minutes
}

function nivo_slider_assets () {
	wp_enqueue_script( 'nivo_slider_js',    get_template_directory_uri() . '/assets/vendor/nivo-slider/jquery.nivo.slider.pack.js', array( 'jquery' ), '3.2', true );
  wp_enqueue_style(  'nivo_slider_css',   get_template_directory_uri() . '/assets/vendor/nivo-slider/nivo-slider.css', false, '3.2');
  wp_enqueue_style(  'nivo_slider_theme', get_template_directory_uri() . '/assets/lib/nivo-slider-sunyit-theme/sunyit.css', false, '3.2');	
}
add_action( 'wp_enqueue_scripts', 'nivo_slider_assets' );

function sunyit_theme_assets () {
	wp_enqueue_script( 'jquery_hoverintent',			 get_template_directory_uri() . '/assets/js/vendor/jquery.hoverIntent.minified.js', array( 'jquery' ), '1.8.0', true );
	wp_enqueue_script( 'bootstrap_hover_dropdown', get_template_directory_uri() . '/assets/js/vendor/bootstrap-hover-dropdown.js', array( 'jquery' ), '2.0.11', true );
}
add_action( 'wp_enqueue_scripts', 'sunyit_theme_assets' );

function shorten($string, $length)
{
		
		if (strlen($string) <= $length) {
			return $string;
		}
		
    // By default, an ellipsis will be appended to the end of the text.
    $suffix = '&hellip;';
 
    // Convert 'smart' punctuation to 'dumb' punctuation, strip the HTML tags,
    // and convert all tabs and line-break characters to single spaces.
    $short_desc = trim(str_replace(array("\r","\n", "\t"), ' ', strip_tags($string)));
 
    // Cut the string to the requested length, and strip any extraneous spaces 
    // from the beginning and end.
    // $desc = trim(substr($short_desc, 0, $length));

    // Cut the string to the requested length, splitting on word boundaries,
    // and strip any extraneous spaces from the beginning and end.
		$desc = current( explode("\n", wordwrap(trim($short_desc), $length, "\n") ));
 
    // Find out what the last displayed character is in the shortened string
    $lastchar = substr($desc, -1, 1);
 
    // If the last character is a period, an exclamation point, or a question 
    // mark, clear out the appended text.
    if ($lastchar == '.' || $lastchar == '!' || $lastchar == '?') $suffix='';
 
    // Append the text.
    $desc .= $suffix;
 
    // Send the new description back to the page.
    return $desc;
}


/* =======================================================================================
	
	Advanced Custom Fields - custom titles and menus for specific sections
	
*/

function get_section_title ( $post_id='' ) {
	
	if (!$post_id) {
		$post_id = get_the_ID();
	}
	
	$title = get_field( 'section_title', $post_id );
	
	if ( $title ) {
		return $title;
	} else {
		$post = get_post( $post_id );
		$parent = $post->post_parent;
		if ($parent == false) {
			return false;
		}
		return get_section_title( $parent );
	}
	
}

function get_section_menu ( $post_id='' ) {
	
	if (!$post_id) {
		$post_id = get_the_ID();
	}
	
	$menu_id = get_field( 'section_menu_id', $post_id );
	
	if ( $menu_id ) {
		return $menu_id;
	} else {
		$post = get_post( $post_id );
		$parent = $post->post_parent;
		if ($parent == false) {
			return false;
		}
		return get_section_menu( $parent );
	}
	
}

if( function_exists('register_field_group') ):

	register_field_group(array (
		'key' => 'group_5461272493fde',
		'title' => 'Section',
		'fields' => array (
			array (
				'key' => 'field_5461273f1f8f5',
				'label' => 'Title',
				'name' => 'section_title',
				'prefix' => '',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_546127f841d40',
				'label' => 'Menu',
				'name' => 'section_menu_id',
				'prefix' => '',
				'type' => 'nav_menu',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'save_format' => 'id',
				'container' => 'nav',
				'allow_null' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'side',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));

endif;
